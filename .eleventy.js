const glob = require("glob");
const path = require("path");
const fs = require("fs");

module.exports = function (eleventyConfig) {
  eleventyConfig.addPlugin(require("@11ty/eleventy-navigation"));
  eleventyConfig.addPassthroughCopy("files");

  eleventyConfig.addFilter("startsWith", (str = "", searchString = "") =>
    str.startsWith(searchString),
  );

  const markdownIt = require("markdown-it");
  const markdownItOptions = {
    html: true,
    linkify: true,
  };

  const filesList = glob.sync("**/*.md");

  const md = markdownIt(markdownItOptions)
    .use(require("markdown-it-footnote"))
    .use(require("markdown-it-attrs"))
    .use(
      require("markdown-it-obsidian-images")({
        relativeBaseURL: "/campaign/images/",
      }),
    )
    .use(require("@cthos/markdown-it-ironvault-parser").default)
    .use(function (md) {
      // Recognize Mediawiki links ([[text]]) so that internal Obsidian Links still work
      md.linkify.add("[[", {
        validate: /^\s?([^\[\]\|\n\r]+)(\|[^\[\]\|\n\r]+)?\s?\]\]/,
        normalize: (match) => {
          const parts = match.raw.slice(2, -2).split("|");
          parts[0] = parts[0].replace(/.(md|markdown)\s?$/i, "");
          match.text = (parts[1] || parts[0]).trim();
          match.url = filesList.find((el) => el.includes(parts[0])) || "/404";
          match.url = `/${match.url.replace(".md", "/")}`;
        },
      });
    });

  eleventyConfig.addFilter("markdownify", (string) => {
    return md.render(string);
  });

  eleventyConfig.setLibrary("md", md);
  eleventyConfig.addPassthroughCopy("campaign/images");
  eleventyConfig.addPassthroughCopy("assets");

  eleventyConfig.addCollection("navPages", (collection) => {
    const nav = {};

    collection
      .getAll()
      .sort((a, b) => {
        return a.url < b.url ? -1 : 1;
      })
      .forEach((cl) => {
        const pathBits = cl.url.split("/");

        pathBits.shift();

        if (pathBits[0] == "") {
          return;
        }

        if (!nav[pathBits[0]]) {
          nav[pathBits[0]] = {
            name: pathBits[0],
            url: `/${pathBits[0]}`,
            children: {},
          };
        }

        let navRef = nav[pathBits[0]];

        pathBits.shift();

        pathBits.forEach((b, i) => {
          const path = cl.url.split("/", i + 3);

          if (b == "") {
            return;
          }

          if (!navRef.children) {
            navRef.children = {};
          }

          if (!navRef.children[b]) {
            navRef.children[b] = {
              name: b,
              url: path.join("/"),
            };
          }

          if (!fs.existsSync(`${__dirname}/${navRef.children[b].url}.md`)) {
            console.log(
              `${navRef.children[b].url}.md is missing. Removing link ref`,
            );
            delete navRef.children[b].url;
          }

          navRef = navRef.children[b];
        });
      });

    // Only renders the contents of the campaign folder.
    return nav.campaign.children;
  });

  return {
    templateFormats: ["md", "njk", "html", "liquid"],
    dir: {
      input: "./",
      includes: ".includes",
      data: ".data",
      output: ".site",
    },
  };
};
