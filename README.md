# Solo TTRPG 11ty Template

Made this to share roughly what I'm doing to share solo campaign work. It's essentially a fork of what I was doing for my regular RP Group (for an example of that, [https://blades.rollnotice.com/](https://blades.rollnotice.com/)).

It assumes you'll be using Obsidian to manage the content, and builds the navigation off of the folder structure of that campaign folder.

## Prerequisites / Quickstart

You'll need to have a reasonably recent node installation - I recommend the LTS version.

After that:

```bash
npm ci
npm run build
```

That'll produce the output in the `.site` directory.

## Basic Structure

Your campaign data should be located in the `campaign` folder, and it.

The general assumption is you'll have a single campaign per website here, but this does support multiple campaigns (if you create them with Iron Vault in the same campaign folder).

Your Obsidian Vault goes in the campaign folder, and whatever folder structure you have will be rendered on the left, using `@11ty/eleventy-navigation`. Obsidian internal links and images should render properly, though there are some weird quirks with the `|size` syntax rendering properly.

## Images

The `.eleventy.js` file assumes that all images across all campaigns live in the `campaign/images` folder. You would need to modify the passthrough and 11ty images plugin to handle per-campaign image folders.

## Navigation

Everything in the `campaign` folder is included in the navigation, currently, as long as it generates html after 11ty is done parsing it. So, the images folder won't show up by default, but if you put an html or markdown file it will.

You can exclude stuff currently by telling 11ty to ignore it via the `.eleventyignore` file.

Sorting the navigation is possible by mucking around in `.eleventy.js`, the `navPages` collection and the resulting eleventyNavigation data.

## Config

There's a `siteConfig.json` file in `.data` which currently only holds the title of your site, which is just being rendered in the header and the `layout.njk` files.

## Styles

All the base css is in `assets/app.css` and the Ironsworn specific stuff is in `assets/ironsworn.css`.

## Fonts

Fonts are being loaded from [Bunny's Font CDN](https://fonts.bunny.net/), in the `.includes/_head.njk` file

## TODOs

- [x] Figure out why the link parser isn't parsing that link in the README.md
- [ ] Get the navigation to handle an index.md for folders with nested content (currently it won't recognize those).
- [ ] Parse the character sheet from the frontmatter data
- [ ] Handle shorthand kdl syntax for rolls (`roll 1 2 1 8 9` for example)
- [ ] Clean up CSS vars

## FAQ

> Well, anticipated FAQs, anyhow

### Wait, Ironsworn / Starforged?

Yes! There's an awesome in-development Obsidian Plugin called [Iron Vault](https://github.com/cwegrzyn/iron-vault), which facilitates a lot of play in Ironsworn. So this repo is half-11ty starter for how I did older campaign sites (see https://blades.rollnotice.com/ for an older example) and a markdown-it plugin to parse the blocks that said plugin produces. That markdown-it plugin uses nunjucks for templates so you can muck around with the markup.

Thanks especially to Zkat for the mechanics blocks (which also uses [KDL](https://kdl.dev/)) and helping me find relevant sections.

Related point, that markdown-it plugin only renders `move` and `meter` blocks, but there are a [ton more](https://cwegrzyn.github.io/iron-vault/blocks/mechanics-blocks.html).

### Why did you include the Obsidian Notebook in the Repo?

<<<<<<< Updated upstream
I wanted you to be able to see it working and what the expectations are. It _also_ includes the .obsidian folder so you can just open it and run. That folder _includes_ the Iron Vault plugin and BRAT. 
=======
I wanted you to be able to see it working and what the expectations are. It _also_ includes the .obsidian folder so you can just open it and run. That folder _includes_ the Ironsworn plugin and BRAT.
>>>>>>> Stashed changes

### Do I need to be playing Ironsworn / Starforged for this to work?

Nope! It'll render anything in that Campaign folder, I've just included a markdown-it filter to handle some of the `mechanics` blocks that the Ironsworn plugin leaves in the markdown.

### Why is Rucks an Astronaut?

While I did license an image from Shutterstock, distributing it as-is from a git repo seemed like a bit much so I went with a CC-BY image instead.
