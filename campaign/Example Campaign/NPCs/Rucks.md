![[46313322352_21f05840a6_c.jpg]]
> "Astronaut Edward H. White II, pilot on the Gemini-Titan IV (GT-4) spaceflight, floats in the zero gravity of space outside the Gemini IV spacecraft. Original from NASA . Digitally enhanced by rawpixel." by Free Public Domain Illustrations by rawpixel is licensed under CC BY 2.0. 

- Historian, Scruffy, Flashy - Greedy, Quirky. Must Avenge a Wrong.

Rucks is a man in his mid 30s, well-kempt with colorful clothing (most often a fine suit in various floral hues) but who pays no attention to his beard or hair.

An ancient ship dropped out out of a jump point near Cauldron, a semi-regular occurrence. Cauldron was set up to salvage the wrecks of ships that mysteriously show up here. This, however, was the oldest and largest ship that’s ever appeared.

Rucks was on one of the initial expeditions into the ship and has made several interesting discoveries. However, word got out and some powerful opportunists have arrived and shoved the locals (at Cauldron) out of being able to explore and salvage - with threat of force / violence. While there are more people at Cauldron, the spacers are more heavily armed.

Rucks is resentful that he’s not been able to come back, and he wants Lyric’s help. Specifically, he wants to sneak aboard the derelict, get some information back, and hopefully remove the spacers.