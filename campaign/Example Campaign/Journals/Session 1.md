> We’ll start In Media Res - the Iron vow will be with Rucks to get onto the ship to recover the ‘Infirnia Codex’, a physical database somewhere in the ship that Rucks found mentions to while probing the database. We’ll make this a dangerous vow and swear it on the blade.

```iron-vault-mechanics
move "[Begin a Session](datasworn:move:starforged\/session\/begin_a_session)"

move "[Swear an Iron Vow](datasworn:move:starforged\/quest\/swear_an_iron_vow)" {
    roll "heart" action=1 adds=0 stat=3 vs1=8 vs2=5
}

```

> I roll +Heart and get a miss - there’s a significant obstacle in the way, an urgent situation that threatens the mission. Let’s say Lyric and Rucks get spotted trying to head to the ship, but they’re clocked as a medical vehicle and the Spacers have a problem. They’ve been infected by …something.

"Oh. Well, that’s not good.” Lyric says idly and glances over at Princess. “I think they spotted us”.

“What? They weren’t supposed to be out here right now, they just left. I’ve been watching them come and go for weeks and they’ve always stuck to the same schedule!” Rucks replies, a tinge of worry in his voice.

The short-range comm crackles and a voice cuts through the static, grainy and distorted. “Medical Vessel, please identify yourself. We have an urgent medical situation and invoke Section 829 of the Covenant.” The voice is clearly starts confident, but falters. “… Please, please help us.”

“This is Baba, Captain of the Medical Vessel **Asclepius**, First Order of the Boundless Heart. Your petition is heard and accepted, what’s the situation?”

“What???” Rucks hisses “You don’t know what they’ll do! You promised to get me on that ship!”

“They invoked Section 829, Rucks. ‘All sworn medical vessels are required to render aid to the best of their ability under protection of hospitality’. They’re not going to shoot us, and I can’t ignore a formal request for aid.”

Lyric slowly turns to stare directly into Rucks’s eyes. Princess, sitting in the copilot’s chair, follows the same motion. Her fur turns from its usual snow white color to a deep crimson.

“Mew.”

A few tense moments pass when the voice comes back over the line. “Oh thank Osiris, I’m sending you coordinates - dock your ship to ours and I’ll explain at the airlock.”

### Entering the Salvager Ship

About 20 minutes later, the Asclepius moves into view of the salvager ship. About 200 meters long, it sports a fresh coat of white paint, and gold trim clearly designed to look luxurious. It could easily be mistaken for a luxury vessel (one of the kinds a Founder Clan might have commissioned, or one of the few pleasure planets) except for the various protrusions from the side designed to pull in smaller salvage, and the numerous docking bays exposed to space for a similar purpose.

As the Asclepius moves closer, it becomes clear that a good portion of the ship isn’t powered - running lights should be clear all over the ship, but they only appear to be on closer to the designated docking bay and a few portions closer to the stern.

The ship slowly moves into the docking bay, and a large blast shutter moves into place. Just a few minutes later a ding appears on the piloting console, indicating that the air pressure outside is stable. Lyric gears up and heads down to meet the mystery voice.

“You planning on staying on the ship or coming along?” Lyric says to Rucks.

“Oh I’m staying _right here_ until you’re done. They don’t need to see me. And I’d appreciate it if you, you know, didn’t mention that I’m here.”

Lyric nods, and puts the medical respirator over her mouth before heading out. Princess trails along a few feet behind, and they head into the ship.

Standing on the landing pad is a short squat woman waiting for Lyric to descend. She looks disheveled, as if she hasn’t slept in days. Her brown hair is draped carelessly over her overalls.

“You must be Baba. I’m glad you’re here. I’m Cybil, head of Engineering… and I suppose the highest ranking crewmember still lucid.”

“The Boundless Heart is always glad to help those in need. What’s the problem?”

“Right. About 3 days ago, we brought back some salvage from inside of the Void Derelict. Shortly after that, something odd started happening. People reported seeing shadows, hearing their dead loved ones, you know - all that stuff you hear about at the start of a good horror holovid. Anyhow, the situation went from bad to _deadly_ real fast. Some folks fell catatonic, some folks turned extremely violent and started attacking their crewmates. The captain’s been gravely injured, and our healer was killed in the initial onslaught. Before he died, he thought it was some sort of pathogen but was unable to confirm it. We’ve set up an infirmary not too far from here for those who are injured or still holding on. A good part of the ship is under quarantine to keep the crazed from causing too much more damage to others… and hopefully themselves. Where do you want to start?”

Princess moves ahead of Lyric, her fur now a mix of blues, purples, and blacks.

“Take me to the infirmary, let’s triage and see if there’s anything I can do about the captain. Either way I need more information.”

### The infirmary

Lyric and Cybil head to the infirmary. Crew members are packed in like sardines in what appears to normally be a mess hall. Several of them are groaning, and the scent of blood and bodily fluid fills the air. Cybil leads Lyric to the Captain, bloody bandages covering his body, one wrapped around his face covering his left eye. His breathing is shallow and it sounds like he might have fluid in his lungs.

“He’s in bad shape. If we don’t do something soon, he’s probably going to die.” Lyric says to Cybil and gets to work.

```iron-vault-mechanics
move "[Heal](datasworn:move:starforged\/recover\/heal)" {
    add 1 "Healer"
    roll "wits" action=5 adds=1 stat=2 vs1=1 vs2=8
}
meter "supply" from=5 to=3
meter "momentum" from=2 to=3
```

> I believe this is a **Heal** move, so I roll +Wits as per providing care. Because Lyric is a healer, I get a +1 to this roll. I get a weak hit. I take the +1 Momentum from the Healer Path, and choose to sacrifice 2 supply, I apparently need to use a lot of resources to get this guy stable.

Lyric carefully removes the bandages and quickly finds a mess of issues: internal bleeding, flesh barely held together, the works. She needs to use her grade 2 medical nanites, and several piles of bandages, but eventually she turns to Cybil.

“I’ve managed to stabilize him. He had a lot of blood in his chest cavity, but the nanites I’ve injected him with will do the internal repairs. He’ll need plenty of food and fluids, but he should pull through. The nanites should deactivate and flush around the same time.”

Cybil sighs and looks visibly relieved. Princess’s fur turns a golden color and she rubs herself against Cybil’s leg.

“Your… glowcat? Seems uncommonly intelligent.”

Princess looks up to her. Her fur shifts quickly back to its normal snow white color.

“Oh. Yes. Princess is probably smarter than me. Great bedside manner, too. I couldn’t ask for a better nurse.”

Lyric looks around at the others. “Show me an affected crew member. I want to take a blood sample.”

```iron-vault-mechanics
move "[Gather Information](datasworn:move:starforged\/adventure\/gather_information)" {
    roll "wits" action=5 adds=0 stat=2 vs1=4 vs2=6
}
meter "momentum" from=3 to=5
```

> I think this is going to be a **Gather Information**, I’m trying to figure out more about the sickness.   Roll +Wits. Very strong hit, +2 momentum and I know what’s happening.

Lyric takes a sample of the crew member’s blood and discovers something slightly horrifying. His blood is filled with nanites, ones that seem to be ancient, and they’re causing havoc in the crew member’s brain. They also appear to be receiving orders from somewhere.

She turns to Cybil. “Cybil, what exactly did you bring on board and where is it now? I think it’s active and we need to shut it down.”

Cybil shifts a bit, and hesitates.

Princess lets out a tiny “mrr”, her fur has changed to a deep blue color.

“Oh, right, I’ve heard Glowcats can detect emotions, so it’d be pointless to lie to you, right?”, Cybil says.

“That’s right. You can’t hide your feelings from her. That’s what makes her the best nurse. Now, what did you bring on board, and are you going to help me get to it?”

```iron-vault-mechanics
move "[Compel](datasworn:move:starforged\/adventure\/compel)" {
    roll "heart" action=5 adds=0 stat=3 vs1=4 vs2=2
}
meter "momentum" from=5 to=6
```

> Well, I’m lying a bit because I do not have the advancement to get a bonus on this **Compel**. I roll +Heart (which is a 3) and get a strong hit. +1 Momentum

“….fine. I don’t know exactly what it is, only that it was about the size of a hoverbike. It should still be down in the hold. I think Dr. Elstock, our resident Archaneologist, set up a temporary station down there to investigate it.”

“Where’s Dr. Elstock now?”

“I don’t know, they’ve not been seen since all of this”, she gestures around the makeshift infirmary “went down.”

Lyric looks down at princess, notes that her fur now white with a golden stripe down her back, and looks back up to Cybil. “Right, and I suppose the only way down to the cargo bay is through the quarantine?”

“How’d you guess? Give me a minute to gear up, I’m coming with you.”

As she turns to leave, Lyric reaches out and grabs her arm.

“Wait.”

She reaches to her belt and pulls out her athame, and places the flat of the blade on her own forehead.

“I swear to you that I will put an end to this affliction, on my honor as a Medicus.”

```iron-vault-mechanics
move "[Swear an Iron Vow](datasworn:move:starforged\/quest\/swear_an_iron_vow)" {
    roll "heart" action=5 adds=0 stat=3 vs1=1 vs2=9
}
```
> Let’s **Swear an Iron Vow** for funsies. It’s dangerous, it shouldn’t take _too long_ but there are a lot of afflicted crew members in the way. Roll +Heart - Weak Hit - +1 Momentum

Cybil pauses for a second, and nods before turning to grab her things.

Lyric looks down to Princess. “You take care of these people while I’m away, try to comfort them.”

“Meeeeeeewwwww”.
## Through the Quarantine

About 30 minutes later, Cybil and Lyric are standing outside of a bulkhead several corridors down from the infirmary. They both take a deep breath. Cybil places her hand on the door controls and looks to Lyric.

“We’re going to have to close this door behind us, and we won’t be able to come back unless we lift the quarantine. Are you ready?”

Lyric nods, and turns toward the door.

Cybil presses her hand on the door scanner, and says “Chief Engineer Cybil Artemesia, security override 8274-gamma. Release Door lock for 45 seconds. Maintain quarantine.”

The door chirps cheerfully and rises slowly. Lyric and Cybil head inside, and the door slowly closes behind them.

The corridor on the other side of the door is quiet, and unremarkable. The pipes lining the ceiling show a little bit of rust, but otherwise there is no sign of commotion. They proceed cautiously towards the stairs that will lead them to the cargo hold.

A few turns later, the team turns the corner and sees a crew member standing in their way, head pressed against the wall, blood dripping from his hands onto the floor.

Lyric instinctively moves to help them, but Cybil holds her back - and makes a “shh” gesture with her finger.

Lyric nods and decides to move closer to him, but slowly and quietly this time.

```iron-vault-mechanics
move "[Face Danger](datasworn:move:starforged\/adventure\/face_danger)" {
    roll "shadow" action=5 adds=0 stat=2 vs1=2 vs2=2
}
```

> I figure this is a **Face Danger**, the crew member could be violent. I think she’s moving “stealthily” so roll +shadow. and I got a strong hit with a twist. Fitting. I’ve got something in mind.

Lyric gets right up to the crew member, who doesn’t seem to notice or acknowledge her until she gets right behind them. He turns and grab Lyric - the his eyes are red, filled with blood, red tear stains dripping down their face. Their mouth is curled upward, as if their lips are pinned to their gums, showing their teeth. Lyric is startled, but then the crew member makes a guttural noise.

“He…lp… me…. Please.”

They drop to the ground, breathing heavily.

Cybil stands several feet back, shocked. “That’s…I didn’t think anyone was still lucid down here.”

“No time for that, we’ve got to help them.”

```iron-vault-mechanics
move "[Heal](datasworn:move:starforged\/recover\/heal)" {
    add 1 "Healer"
    roll "heart" action=2 adds=1 stat=3 vs1=6 vs2=8
}
```

> Feels like another **Heal** move to me, so let’s do it. I’m providing care so that’s +wits. Miss.

Lyric gets to work, starting with a low dose of recovery nanites to stop the bleeding. Seconds after injecting them, their body begins to convulse violently and they begin to spit up more blood, splashing Lyric’s suit. A few moments later, they stop seizing and fall silent.

“What the hell just happened?” Cybil says in a loud whisper, trying not to shout.

“I… I don’t know. Those were just a low dose of medical nanites it shouldn’t have….” Lyric trails off, the realization hitting her. “The ancient nanites. They probably interacted with the medical nanites. Dammit.”

She checks the crew member’s pulse once again, and finds nothing. She sighs through her mask, and hopes that none of the ancient nanites in their blood have managed to infect her.

She stands, and hangs her head. Cybil nods, and leads Lyric further down the hallway.

The camera pans, as Cybil and Lyric walk towards the end of the hall. We can see the crew member rise to their feet in a lurch, unnaturally as if they were being pulled up like a marionette.

```iron-vault-mechanics
move "[End a Session](datasworn:move:starforged\/session\/end_a_session)"
```
